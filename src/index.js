import ApexCharts from 'apexcharts'
import 'bootstrap/dist/css/bootstrap.min.css'
import './style.css'
import 'bootstrap'
import $ from 'jquery'

const canvas = document.getElementById("canvas");
const ctx = canvas.getContext('2d');
const playButton = document.getElementById("start");
const resetButton = document.getElementById("reset");
const voltageSlider = document.getElementById("voltage");
const voltageLabel = document.getElementById("voltage-label");
const distanceSlider = document.getElementById("distance");
const distanceLabel = document.getElementById("distance-label");
const lengthSlider = document.getElementById("length");
const lengthLabel = document.getElementById("length-label");
const velocityInput = document.getElementById("velocity");


const chartSwitch = document.getElementById("chartSwitch");
const vectorSwitch = document.getElementById("vectorSwitch");
const nav = document.getElementById("myTab");

ctx.canvas.height = canvas.offsetHeight;
ctx.canvas.width = canvas.offsetWidth;


window.addEventListener("resize", ev => {
    ctx.canvas.height = canvas.offsetHeight;
    ctx.canvas.width = canvas.offsetWidth;
});


voltageSlider.addEventListener("input", ev => {
    u = parseFloat(ev.target.value);
    voltageLabel.innerText = "Напряжение " + ev.target.value + " В";
    drawAll();
})

distanceSlider.addEventListener("input", ev => {
    d = parseFloat(ev.target.value) / 1000;
    distanceLabel.innerText = "Расстояние между пластинами " + ev.target.value + " мм";
    drawAll();
})

lengthSlider.addEventListener("input", ev => {
    l = parseFloat(ev.target.value) / 1000;
    lengthLabel.innerText = "Длина пластин " + ev.target.value + " мм";
    drawAll();
})

velocityInput.addEventListener("input", ev => {
    v0 = parseFloat(ev.target.value) * 1e6;
    drawAll();
})

chartSwitch.addEventListener("change", ev => {
    showChart = !showChart;
    updateSeries();
});

vectorSwitch.addEventListener("change", ev => {
    showVector = !showVector;
    drawAll();
})

distanceSlider.max = canvas.height;
lengthSlider.max = canvas.width;

nav.addEventListener("click", ev => {
    updateSeries();
});

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    updateSeries();
})

playButton.addEventListener("click", ev => {
    updateSeries();
    updateData();
    if (animationId == null) {
        play();
    } else {
        pause();
    }
})

resetButton.addEventListener("click", ev => {
    t = 0;
    // angleSlider.disabled = false;
    // tensionInput.disabled = false;
    voltageSlider.disabled = false;
    distanceSlider.disabled = false;
    lengthSlider.disabled = false;
    velocityInput.disabled = false;
    playButton.disabled = false;
    accData = [];
    speedData = [];
    positionData = [];
    updateSeries();
    pause();
    drawAll();
})


let lastTime = performance.now();
let animationId = null;
let t = 0;

let showChart = false;
let showVector = false;

let accData = [];
let speedData = [];
let positionData = [];

let e = 1.602e-19;
let m = 9.109e-31;
let v0 = 1e6;
let u = 30;
let d = 0.05;
let l = 0.2;

const getE = () => {
    return u / d;
}

const getForce = () => {
    return getE() * e;
}

const getA = () => {
    return getForce() / m;
}

const getNowA = () => {
    if (t >= getReachT() && t <= getOutT()) return getA();
    return 0;
}

const getV = t => {
    if (t >= getOutT()) return getA() * (getOutT() - getReachT());
    if (t >= getReachT()) return getA() * (t - getReachT());
    else return 0;
}

const getX = t => {
    if (t >= getOutT()) return getA() * (getOutT() - getReachT()) ** 2 / 2 + getA() * (getOutT() - getReachT()) * (t - getOutT());
    if (t >= getReachT()) return  getA() * (t - getReachT()) ** 2 / 2;
     return 0;
}

const getY = t => {
    return v0 * t;
}

const getMaxT = () => {
    return getReachT() + Math.min(Math.sqrt(2 * (canvas.height / 2000) / getA()), canvas.width / (v0 * 1000));
}

const getReachT = () => {
    return (canvas.width / 2000 - l / 2) / v0;
}

const getOutT = () => {
    return (canvas.width / 2000 + l / 2) / v0;
}

const play = () => {
    lastTime = performance.now();
    playButton.textContent = "Пауза";
    animationId = requestAnimationFrame(renderLoop);
    voltageSlider.disabled = true;
    distanceSlider.disabled = true;
    velocityInput.disabled = true;
    lengthSlider.disabled = true;
    accChart.updateOptions({
        xaxis: {max: getMaxT()},
        yaxis: {
            min: 0,
            max: Math.ceil(2 * getA()),
            decimalsInFloat: 2,
            title: {
                text: "a(м/с^2)"
            },
        }
    });
    speedChart.updateOptions({
        xaxis: {max: getMaxT()},
        yaxis: {
            min: 0,
            max: Math.ceil(getV(getMaxT())),
            decimalsInFloat: 2,
            title: {
                text: "v(м/с)"
            },
        }
    });
    positionChart.updateOptions({
        xaxis: {max: getMaxT()},
        yaxis: {
            min: 0,
            max: getX(getMaxT()),
            decimalsInFloat: 2,
            title: {
                text: "x(м)"
            },
        }
    });
}

const pause = () => {
    playButton.textContent = "Старт";
    cancelAnimationFrame(animationId);
    animationId = null;
}

const updateSeries = () => {
    accChart.updateSeries([{
        data: accData
    }]);
    speedChart.updateSeries([{
        data: speedData
    }]);
    positionChart.updateSeries([{
        data: positionData
    }]);
}

const drawTime = () => {
    ctx.fillStyle = "#000000";
    ctx.fillText("t = " + (t * 10e6).toFixed(3) + " 10e-6 c", 20, 10);
}

const drawGrid = () => {
    ctx.beginPath();
    ctx.translate(0, canvas.height / 2);
    for (let i = 0; i < getMaxT(); i += 1e-9) {
        ctx.lineTo(getY(i) * 1000, getX(i) * 1000);
    }
    ctx.stroke();
    ctx.translate(0, -canvas.height / 2);
}

const drawAll = () => {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.translate(0, canvas.height / 2);
    ctx.beginPath();
    ctx.arc(getY(t) * 1000, getX(t) * 1000, 10, 0, 2 * Math.PI);
    ctx.fillStyle = "#60b9ff";
    ctx.stroke();
    ctx.fill();
    ctx.translate(0, -canvas.height / 2);


    ctx.beginPath();
    ctx.moveTo(canvas.width / 2, 0);
    ctx.lineTo(canvas.width / 2, canvas.height / 2 - d * 1000 / 2);
    ctx.moveTo(canvas.width / 2 - l * 1000 / 2, canvas.height / 2 - d * 1000 / 2);
    ctx.lineTo(canvas.width / 2 + l * 1000 / 2, canvas.height / 2 - d * 1000 / 2);

    ctx.moveTo(canvas.width / 2, canvas.height);
    ctx.lineTo(canvas.width / 2, canvas.height / 2 + d * 1000 / 2);
    ctx.moveTo(canvas.width / 2 - l * 1000 / 2, canvas.height / 2 + d * 1000 / 2);
    ctx.lineTo(canvas.width / 2 + l * 1000 / 2, canvas.height / 2 + d * 1000 / 2);

    ctx.stroke();

    drawTime();
    if (showVector) {
        drawGrid();
    }

}


const renderLoop = () => {
    let dt = (performance.now() - lastTime);
    t += dt / 1e10;

    drawAll();
    lastTime = performance.now();

    console.log(getMaxT());
    if (t > getMaxT()) {
        pause();
        playButton.disabled = true;
    } else {
        animationId = requestAnimationFrame(renderLoop);
    }
};

drawAll();
pause();


function updateData() {
    accData.push({x: t, y: getNowA(t)});
    speedData.push({x: t, y: getV(t)});
    positionData.push({x: t, y: getX(t)});
}

let options = {
    chart: {
        id: 'realtime',
        height: 350,
        type: 'line',
        animations: {
            enabled: true,
            easing: 'linear',
            dynamicAnimation: {
                speed: 1000
            }
        },
        toolbar: {
            show: false
        },
        zoom: {
            enabled: false
        }
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        curve: 'straight'
    },
    markers: {
        size: 0
    },
    xaxis: {
        type: 'numeric',
        min: 0,
        title: {
            text: "t(c) 1e-6",
            offsetY: 10,
        },
        labels: {
            formatter: function (value) {
                return (value * 1e6).toFixed(3);
            }
        }
    },
    legend: {
        show: false
    },
};

let accOptions = {
    series: [{
        data: accData.slice()
    }],
    yaxis: {
        min: 0,
        title: {
            text: "a(м/с^2)"
        },

    },
    ...options
};
let speedOptions = {
    series: [{
        data: speedData.slice()
    }],
    yaxis: {
        min: 0,
        title: {
            text: "v(м/с)"
        },
    },
    ...options
};
let positionOptions = {
    series: [{
        data: positionData.slice()
    }],
    yaxis: {
        min: 0,
        title: {
            text: "x(м)"
        },
    },
    ...options
}


let positionChart = new ApexCharts(document.querySelector("#position-chart"), positionOptions);
let speedChart = new ApexCharts(document.querySelector("#speed-chart"), speedOptions);
let accChart = new ApexCharts(document.querySelector("#acc-chart"), accOptions);


positionChart.render();
speedChart.render();
accChart.render();

window.setInterval(function () {
    if (animationId != null) {
        updateData();
        if (showChart) {
            updateSeries();
        }
    }
}, 100)





